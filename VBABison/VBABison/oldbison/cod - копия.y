%union {
    int int_value;
    char char_value;
    char* string_value;
    
    struct Expression* expr;
    struct Statement* stmt;
    struct List* list;
}
%type <expr> expression
%type <expr> identifier
%type <expr> array_slice
%type <expr> parameter

%type <stmt> statement
%type <stmt> function_definition
%type <stmt> condition_statement
%type <stmt> elif_statement
%type <stmt> while_statement
%type <stmt> for_statement
%type <stmt> return_statement

%type <list> program
%type <list> arguments_e
%type <list> arguments
%type <list> statement_list
%type <list> suite
%type <list> parameters_e
%type <list> parameters
%type <list> elif_statement_list

%token <int_value> INT
%token <char_value> CHAR
%token <string_value> STRING
%token <string_value> ID

%token FALSE
%token IF
%token FROM
%token IN
%token THEN
%token AS
%token RETURN
%token WHILE
%token FOR
%token TRUE
%token BREAK
%token ELIF
%token ELSE
%token DIM

%token NEWLINE

%token END_OF_FILE

%right '=' PLUS_ASSIGN MINUS_ASSIGN MULT_ASSIGN POW_ASSIGN DIV_ASSIGN MOD_ASSIGN
%left '<' LESSER_EQUAL '>' GREATER_EQUAL NOT_EQUAL EQUAL
%left '+' '-'
%left '*' '/' '%' FLOOR_DIV
%left UMINUS
%nonassoc ')'
%nonassoc ']'
%nonassoc '}'

%%

program : statement_list   											 { $$ = head = $1; printf("START"); }
   	 ;
	 
expression  : | expression '<' expression   								 { $$ = createExpression(ET_LESSER, $1, $3); }
   		 | expression LESSER_EQUAL expression   					 { $$ = createExpression(ET_LESSER_EQUAL, $1, $3); }
   		 | expression '>' expression   								 { $$ = createExpression(ET_GREATER, $1, $3); }
   		 | expression NOT_EQUAL expression   						 { $$ = createExpression(ET_NOT_EQUAL, $1, $3); }
   		 | expression EQUAL expression   							 { $$ = createExpression(ET_EQUAL, $1, $3); }
   		 | expression '+' expression   								 { $$ = createExpression(ET_PLUS, $1, $3); }
   		 | expression '-' expression   								 { $$ = createExpression(ET_MINUS, $1, $3); }
   		 | expression '*' expression   								 { $$ = createExpression(ET_MULT, $1, $3); }
   		 | expression '/' expression   								 { $$ = createExpression(ET_DIV, $1, $3); }
   		 | '+' expression %prec UPLUS   							 { $$ = createExpression(ET_UPLUS, $2, NULL); }
   		 | '-' expression %prec UMINUS   							 { $$ = createExpression(ET_UMINUS, $2, NULL); }
   		 | '(' expression ')'   									 { $$ = $2; }
   		 | identifier   											 { $$ = $1; }
   		 | INT   													 { $$ = createConstExpr($1) }
   		 | CHAR   													 { $$ = createConstExpr($1) }
   		 | STRING   												 { $$ = createConstExpr($1) }
		 ///////////////////??????????????????
   		 | '[' arguments_e ']'   											 { $$ = $2 }
   		 | expression '[' expression ']'   									 { $$ = createExpression(ET_ARRAY_APPEAL, $1, $3); }
   		 | expression '=' expression   										 { $$ = createExpression(ET_ASSIGN, $1, $3); }
		 ///////////////////??????????????????
   		 | expression '(' arguments_e ')'   								 { $$ = $2 }
   		 ;
		 
identifier  : ID   													 { $$ = createIdExpr($1) }
   		 ;

arguments_e    : arguments   										 { $$ = $1; }
   		 | arguments ','   											 { $$ = $1; }
   		 |    														 { $$ = NULL; }
   		 ;

arguments    : expression   										 { $$ = createList(???, $1, NULL); }
   		 | arguments ',' expression   								 { $$ = appendToList($1, $3, NULL); }
   		 ;
		 
return_statement    : RETURN expression   								 { $$ = createReturnStatement($2); }

condition_statement :

for_statement :

while_statemnt :