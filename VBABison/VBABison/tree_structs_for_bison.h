#ifndef SIMPLE_TREE_STRUCTS
#define SIMPLE_TREE_STRUCTS
enum ExprType {Int_const, Id, Str_const, Plus, Minus, Mul, Div, As, LT, GT, UMinus, Integer, String, LTOE, GTOE, NQ, Mass};
enum StmtType {Ex, If, AltIf, While, Write, Read, Sub, Fun_Integer, Fun_String, Integer_stmt, String_stmt, Return_stmt};

struct Expression {
enum ExprType Type;
int Num;
char *Name;
char *Str;
struct Expression *Left;
struct Expression *Right;
struct Expression *Next;
struct ExpressionList * ExprList;
};

struct StatementList {
struct Statement *First;
struct Statement *Last;
};

struct ExpressionList {
struct Expression *First;
struct Expression *Last;
};

struct Statement {
enum StmtType Type;
char *Name;
char *Str;
int Num;
struct Expression *Expr;
struct StatementList *Block;
struct StatementList *Arg;
struct StatementList *ElseIfList;
struct Statement *ElseStmt;
struct Statement *Next;
};

struct Program
{
	struct StatementsList *stmts;
};
#endif
