
/*  A Bison parser, made from forbison.y with Bison version GNU Bison version 1.24
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	INT	258
#define	ID	259
#define	STRING_VB_CONST	260
#define	ENDL	261
#define	IF	262
#define	THEN	263
#define	END	264
#define	ELSE	265
#define	ELSEIF	266
#define	WHILE	267
#define	AS	268
#define	DIM	269
#define	INTEGER	270
#define	STRING_VB	271
#define	FUNCTION_VB	272
#define	SUB	273
#define	CONSOLE	274
#define	READLINE	275
#define	WRITELINE	276
#define	RETURN	277
#define	GREATER_THAN_OR_EQUAL	278
#define	LESS_THAN_OR_EQUAL	279
#define	NOT_EQUAL	280
#define	UMINUS	281

#line 1 "forbison.y"

# include "stdafx.h"
#include "tree_structs_for_bison.h"
#include <stdio.h>
#include "print_graph.h"
#include <malloc.h>
void yyerror(char const *s);
extern FILE *yyin;
extern int yylex(void);
struct Expression * create_const_expr(int token);
struct Expression * create_id_expr(char * token);
struct Expression * create_string_expr(char * token);
struct Expression * create_mass_expr(char * token, int num);
struct Expression * create_comp_expr(enum ExprType type, char * token, int * num, char * str);
struct Expression * create_fun_expr(char *token, struct ExpressionList *seq);
struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs);
struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr);
struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block, struct StatementList *elseif_block, struct Statement *else_stmt);
struct Statement * create_sub_stmt(enum StmtType type, char * name ,struct StatementList *arg, struct StatementList *block);
struct Statement * create_dim_stmt(enum StmtType type, char * token);
struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block);
struct Statement * create_fun_stmt(char * name ,struct StatementList *arg, enum StmtType type1, struct StatementList *block);
struct Statement * create_console(enum StmtType type, struct Expression * e1);
struct Statement * create_else_stmt(struct Expression *expr, struct StatementList *block);
struct Statement * create_mass_stmt(enum StmtType type, char *token, int *num);
struct StatementList * create_stmt_list(struct Statement *s1);
struct StatementList * add_to_stmt_list(struct StatementList *s1, struct Statement *s2);
struct StatementList * create_fun_list(struct Statement *s1);
struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2);
struct StatementList * create_elseif_list(struct Statement *s1);
struct StatementList * add_to_elseif_list(struct StatementList *s1, struct Statement *s2);
struct ExpressionList * add_to_expr_seq(struct ExpressionList *e1, struct Expression *e2);
struct ExpressionList * create_expr_seq(struct Expression *e1);
struct Program *create_program(struct StatementList *lst);
struct Program *Prg;

#line 37 "forbison.y"
typedef union {
int Int;
char *Id;
char *String;
char* str;
int ival;
struct Expression *Expr;
struct StatementList *SL;
struct Statement *Stmt;
struct Program *Prg;
struct ExpressionList *EL;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		173
#define	YYFLAG		-32768
#define	YYNTBASE	38

#define YYTRANSLATE(x) ((unsigned)(x) <= 281 ? yytranslate[x] : 57)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    34,
    35,    31,    29,    36,    30,    37,    32,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    27,
    26,    28,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    33
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     4,     7,     9,    12,    15,    18,    20,    22,
    24,    26,    30,    33,    35,    37,    39,    41,    43,    47,
    51,    55,    59,    63,    67,    71,    75,    79,    83,    88,
    92,    97,   101,   104,   108,   112,   116,   120,   124,   128,
   132,   136,   140,   144,   147,   153,   159,   167,   175,   183,
   191,   193,   197,   208,   218,   221,   223,   229,   233,   234,
   242,   246,   250,   254,   256,   264,   271,   284,   297,   300
};

static const short yyrhs[] = {    39,
     0,    56,     0,    39,    56,     0,    41,     0,    40,    41,
     0,    40,     6,     0,    42,     6,     0,    47,     0,    51,
     0,    54,     0,    55,     0,    22,    42,     6,     0,    22,
     6,     0,    44,     0,    45,     0,     3,     0,     4,     0,
     5,     0,    42,    26,    42,     0,    42,    29,    42,     0,
    42,    30,    42,     0,    42,    31,    42,     0,    42,    32,
    42,     0,    42,    28,    42,     0,    42,    27,    42,     0,
    42,    23,    42,     0,    42,    24,    42,     0,    42,    25,
    42,     0,     4,    34,    46,    35,     0,     4,    34,    35,
     0,     4,    34,     3,    35,     0,    34,    42,    35,     0,
    30,    42,     0,     4,    26,     3,     0,     4,    26,     5,
     0,     4,    26,     4,     0,     4,    28,     3,     0,     4,
    27,     3,     0,     4,    23,     3,     0,     4,    24,     3,
     0,     4,    25,     4,     0,     4,    25,     3,     0,     4,
    25,     5,     0,    43,     6,     0,    14,     4,    13,    15,
     6,     0,    14,     4,    13,    16,     6,     0,    14,     4,
    13,    15,    26,     3,     6,     0,    14,     4,    13,    16,
    26,     5,     6,     0,    14,     4,    34,     3,    35,    13,
    15,     0,    14,     4,    34,     3,    35,    13,    16,     0,
    42,     0,    46,    36,    42,     0,     7,    43,     8,     6,
    40,    48,    50,     9,     7,     6,     0,     7,    43,     8,
     6,    40,    50,     9,     7,     6,     0,    48,    49,     0,
    49,     0,    11,    43,     8,     6,    40,     0,    10,     6,
    40,     0,     0,    12,    43,     6,    40,     9,    12,     6,
     0,     4,    13,    15,     0,     4,    13,    16,     0,    53,
    36,    52,     0,    52,     0,    19,    37,    21,    34,    42,
    35,     6,     0,    19,    37,    20,    34,    35,     6,     0,
    17,     4,    34,    53,    35,    13,    15,     6,    40,     9,
    17,     6,     0,    17,     4,    34,    53,    35,    13,    16,
     6,    40,     9,    17,     6,     0,    56,     6,     0,    18,
     4,    34,    53,    35,     6,    40,     9,    18,     6,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    84,    86,    87,    89,    90,    91,    93,    94,    95,    96,
    97,    98,    99,   100,   101,   103,   104,   105,   106,   107,
   108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
   118,   119,   120,   122,   123,   124,   125,   126,   127,   128,
   129,   130,   131,   132,   134,   135,   136,   137,   139,   140,
   142,   143,   145,   146,   148,   149,   151,   153,   154,   156,
   158,   159,   161,   162,   164,   166,   168,   169,   170,   171
};

static const char * const yytname[] = {   "$","error","$undefined.","INT","ID",
"STRING_VB_CONST","ENDL","IF","THEN","END","ELSE","ELSEIF","WHILE","AS","DIM",
"INTEGER","STRING_VB","FUNCTION_VB","SUB","CONSOLE","READLINE","WRITELINE","RETURN",
"GREATER_THAN_OR_EQUAL","LESS_THAN_OR_EQUAL","NOT_EQUAL","'='","'<'","'>'","'+'",
"'-'","'*'","'/'","UMINUS","'('","')'","','","'.'","program","fun_list","stmt_list",
"stmt","expr","comp_expr","dim_stmt","mass_stmt","expr_seq","if_stmt","elseif_list",
"elseif_stmt","else_stmt","while_stmt","arg_fun","arg_list","print_stmt","scanf_stmt",
"fun_stmt",""
};
#endif

static const short yyr1[] = {     0,
    38,    39,    39,    40,    40,    40,    41,    41,    41,    41,
    41,    41,    41,    41,    41,    42,    42,    42,    42,    42,
    42,    42,    42,    42,    42,    42,    42,    42,    42,    42,
    42,    42,    42,    43,    43,    43,    43,    43,    43,    43,
    43,    43,    43,    43,    44,    44,    44,    44,    45,    45,
    46,    46,    47,    47,    48,    48,    49,    50,    50,    51,
    52,    52,    53,    53,    54,    55,    56,    56,    56,    56
};

static const short yyr2[] = {     0,
     1,     1,     2,     1,     2,     2,     2,     1,     1,     1,
     1,     3,     2,     1,     1,     1,     1,     1,     3,     3,
     3,     3,     3,     3,     3,     3,     3,     3,     4,     3,
     4,     3,     2,     3,     3,     3,     3,     3,     3,     3,
     3,     3,     3,     2,     5,     5,     7,     7,     7,     7,
     1,     3,    10,     9,     2,     1,     5,     3,     0,     7,
     3,     3,     3,     1,     7,     6,    12,    12,     2,    10
};

static const short yydefact[] = {     0,
     0,     0,     1,     2,     0,     0,     3,    69,     0,     0,
     0,    64,     0,     0,     0,     0,     0,     0,    61,    62,
     0,    63,     0,     0,     0,    16,    17,    18,     0,     0,
     0,     0,     0,     0,     0,     0,     4,     0,    14,    15,
     8,     9,    10,    11,     0,     0,     0,     0,     0,     0,
     0,     0,    13,     0,    33,     0,     6,     0,     5,     7,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    16,    30,    51,     0,     0,     0,     0,     0,
     0,     0,    44,     0,    44,     0,     0,     0,     0,    12,
    32,     0,    26,    27,    28,    19,    25,    24,    20,    21,
    22,    23,     0,     0,    31,    29,     0,    39,    40,    42,
    41,    43,    34,    36,    35,    38,    37,     0,     0,     0,
     0,     0,     0,     0,    70,     0,     0,    52,    59,     0,
    45,     0,    46,     0,     0,     0,     0,    67,    68,     0,
     0,    59,    56,     0,     0,     0,     0,     0,    66,     0,
     0,     0,    55,     0,     0,    60,    47,    48,    49,    50,
    65,    58,     0,     0,     0,     0,     0,    54,    57,    53,
     0,     0,     0
};

static const short yydefgoto[] = {   171,
     3,    36,    37,    38,    49,    39,    40,    76,    41,   142,
   143,   144,    42,    12,    13,    43,    44,     4
};

static const short yypact[] = {    -5,
     1,    19,    -5,    26,   -25,     0,    26,-32768,    48,    48,
    60,-32768,    23,    35,    24,    89,    48,    57,-32768,-32768,
    66,-32768,   229,    73,   103,-32768,    70,-32768,   108,   108,
   111,    88,    62,   114,   114,    94,-32768,   241,-32768,-32768,
-32768,-32768,-32768,-32768,   229,   229,     3,    -6,    18,   117,
    -9,    74,-32768,   251,-32768,   261,-32768,   109,-32768,-32768,
   114,   114,   114,   114,   114,   114,   114,   114,   114,   114,
   131,   163,   104,-32768,   287,    75,   143,   144,    82,    86,
   146,   148,-32768,   149,   229,   105,   151,   118,   122,-32768,
-32768,   152,    46,    46,    46,   287,    46,    46,    98,    98,
-32768,-32768,   140,   142,-32768,-32768,   114,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,   229,   195,     4,
     5,   125,   127,   114,-32768,   157,   158,   287,    50,   159,
-32768,   170,-32768,   169,   165,   173,   274,-32768,-32768,   174,
   108,   121,-32768,   167,   175,   177,   178,   126,-32768,   180,
   229,    21,-32768,   179,   182,-32768,-32768,-32768,-32768,-32768,
-32768,   216,   181,   183,   185,   229,   186,-32768,   216,-32768,
   194,   196,-32768
};

static const short yypgoto[] = {-32768,
-32768,   -44,   -36,   -19,   -27,-32768,-32768,-32768,-32768,-32768,
    53,    61,-32768,   188,   198,-32768,-32768,   203
};


#define	YYLAST		319


static const short yytable[] = {    59,
    71,    72,    50,    86,     5,    73,    27,    28,     9,   131,
   133,     1,     2,    54,    55,    56,    77,    78,    79,    80,
    81,    82,     6,    83,    87,    84,    83,    75,   163,   132,
   134,     8,    34,    10,    59,    59,    35,    74,    19,    20,
   119,    93,    94,    95,    96,    97,    98,    99,   100,   101,
   102,    11,    26,    27,    28,    57,    29,    16,    17,   140,
   141,    30,    23,    31,    26,    27,    28,    53,    32,    18,
    17,    33,    15,   129,    67,    68,    69,    70,    45,    34,
    24,    25,    59,    35,   110,   111,   112,   128,   113,   114,
   115,    34,    59,    88,    89,    35,    26,    27,    28,    57,
    29,    21,    58,    47,   137,    30,   162,    31,    46,   106,
   107,    48,    32,   152,    51,    33,    26,    27,    28,   120,
   121,   169,    85,    34,    52,    59,    92,    35,    69,    70,
   140,   141,    59,    26,    27,    28,    57,    29,   105,   103,
   159,   160,    30,    34,    31,   108,   109,    35,   116,    32,
   117,   123,    33,   122,   118,   124,   126,   125,   127,   135,
    34,   136,   138,   139,    35,    26,    27,    28,    57,    29,
   145,   104,   146,   147,    30,   155,    31,   148,   149,   151,
   156,    32,   157,   158,    33,   161,   166,   164,   165,   167,
   168,   170,    34,   172,   153,   173,    35,    26,    27,    28,
    57,    29,   154,   130,    22,     7,    30,    14,    31,     0,
     0,     0,     0,    32,     0,     0,    33,     0,    26,    27,
    28,    57,    29,     0,    34,     0,     0,    30,    35,    31,
     0,    26,    27,    28,    32,    29,     0,    33,     0,     0,
    30,     0,    31,     0,     0,    34,    60,    32,     0,    35,
    33,     0,     0,     0,     0,     0,    90,     0,    34,     0,
     0,     0,    35,    61,    62,    63,    64,    65,    66,    67,
    68,    69,    70,    61,    62,    63,    64,    65,    66,    67,
    68,    69,    70,    61,    62,    63,    64,    65,    66,    67,
    68,    69,    70,     0,     0,    91,    61,    62,    63,    64,
    65,    66,    67,    68,    69,    70,     0,     0,   150,    61,
    62,    63,    64,    65,    66,    67,    68,    69,    70
};

static const short yycheck[] = {    36,
    45,    46,    30,    13,     4,     3,     4,     5,    34,     6,
     6,    17,    18,    33,    34,    35,    23,    24,    25,    26,
    27,    28,     4,     6,    34,     8,     6,    47,     8,    26,
    26,     6,    30,    34,    71,    72,    34,    35,    15,    16,
    85,    61,    62,    63,    64,    65,    66,    67,    68,    69,
    70,     4,     3,     4,     5,     6,     7,    35,    36,    10,
    11,    12,     6,    14,     3,     4,     5,     6,    19,    35,
    36,    22,    13,   118,    29,    30,    31,    32,     6,    30,
    15,    16,   119,    34,     3,     4,     5,   107,     3,     4,
     5,    30,   129,    20,    21,    34,     3,     4,     5,     6,
     7,    13,     9,    34,   124,    12,   151,    14,     6,    35,
    36,     4,    19,   141,     4,    22,     3,     4,     5,    15,
    16,   166,     6,    30,    37,   162,    18,    34,    31,    32,
    10,    11,   169,     3,     4,     5,     6,     7,    35,     9,
    15,    16,    12,    30,    14,     3,     3,    34,     3,    19,
     3,    34,    22,     3,     6,    34,    17,     6,    17,    35,
    30,    35,     6,     6,    34,     3,     4,     5,     6,     7,
    12,     9,     3,     5,    12,     9,    14,    13,     6,     6,
     6,    19,     6,     6,    22,     6,     6,     9,     7,     7,
     6,     6,    30,     0,   142,     0,    34,     3,     4,     5,
     6,     7,   142,     9,    17,     3,    12,    10,    14,    -1,
    -1,    -1,    -1,    19,    -1,    -1,    22,    -1,     3,     4,
     5,     6,     7,    -1,    30,    -1,    -1,    12,    34,    14,
    -1,     3,     4,     5,    19,     7,    -1,    22,    -1,    -1,
    12,    -1,    14,    -1,    -1,    30,     6,    19,    -1,    34,
    22,    -1,    -1,    -1,    -1,    -1,     6,    -1,    30,    -1,
    -1,    -1,    34,    23,    24,    25,    26,    27,    28,    29,
    30,    31,    32,    23,    24,    25,    26,    27,    28,    29,
    30,    31,    32,    23,    24,    25,    26,    27,    28,    29,
    30,    31,    32,    -1,    -1,    35,    23,    24,    25,    26,
    27,    28,    29,    30,    31,    32,    -1,    -1,    35,    23,
    24,    25,    26,    27,    28,    29,    30,    31,    32
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 192 "bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#else
#define YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#endif

int
yyparse(YYPARSE_PARAM)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 84 "forbison.y"
{Prg=create_program(yyvsp[0].SL); yyval.Prg=yyvsp[0].SL;;
    break;}
case 2:
#line 86 "forbison.y"
{yyval.SL = create_fun_list(yyvsp[0].Stmt);;
    break;}
case 3:
#line 87 "forbison.y"
{yyval.SL = add_to_fun_list(yyvsp[-1].SL, yyvsp[0].Stmt);;
    break;}
case 4:
#line 89 "forbison.y"
{yyval.SL=create_stmt_list(yyvsp[0].Stmt);;
    break;}
case 5:
#line 90 "forbison.y"
{yyval.SL=add_to_stmt_list (yyvsp[-1].SL, yyvsp[0].Stmt);;
    break;}
case 6:
#line 91 "forbison.y"
{yyval.SL=yyvsp[-1].SL;;
    break;}
case 7:
#line 93 "forbison.y"
{yyval.Stmt=create_simple_stmt(Ex,yyvsp[-1].Expr);;
    break;}
case 8:
#line 94 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 9:
#line 95 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 10:
#line 96 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 11:
#line 97 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 12:
#line 98 "forbison.y"
{yyval.Stmt=create_simple_stmt(Return_stmt, yyvsp[-1].Expr);;
    break;}
case 13:
#line 99 "forbison.y"
{yyval.Stmt=create_simple_stmt(Return_stmt, NULL);;
    break;}
case 14:
#line 100 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 15:
#line 101 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 16:
#line 103 "forbison.y"
{yyval.Expr=create_const_expr(yyvsp[0].Int);;
    break;}
case 17:
#line 104 "forbison.y"
{yyval.Expr=create_id_expr(yyvsp[0].Id);;
    break;}
case 18:
#line 105 "forbison.y"
{yyval.Expr=create_string_expr(yyvsp[0].String);;
    break;}
case 19:
#line 106 "forbison.y"
{yyval.Expr=create_op_expr(As,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 20:
#line 107 "forbison.y"
{yyval.Expr=create_op_expr(Plus,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 21:
#line 108 "forbison.y"
{yyval.Expr=create_op_expr(Minus,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 22:
#line 109 "forbison.y"
{yyval.Expr=create_op_expr(Mul,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 23:
#line 110 "forbison.y"
{yyval.Expr=create_op_expr(Div,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 24:
#line 111 "forbison.y"
{yyval.Expr=create_op_expr(GT,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 25:
#line 112 "forbison.y"
{yyval.Expr=create_op_expr(LT,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 26:
#line 113 "forbison.y"
{yyval.Expr=create_op_expr(GTOE,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 27:
#line 114 "forbison.y"
{yyval.Expr=create_op_expr(LTOE,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 28:
#line 115 "forbison.y"
{yyval.Expr=create_op_expr(NQ,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 29:
#line 116 "forbison.y"
{yyval.Expr=create_fun_expr(yyvsp[-3].Id, yyvsp[-1].EL);;
    break;}
case 30:
#line 117 "forbison.y"
{yyval.Expr=create_fun_expr(yyvsp[-2].Id, NULL);;
    break;}
case 31:
#line 118 "forbison.y"
{yyval.Expr=create_mass_expr(yyvsp[-3].Id, yyvsp[-1].Int);;
    break;}
case 32:
#line 119 "forbison.y"
{yyval.Expr=yyvsp[-1].Expr;;
    break;}
case 33:
#line 120 "forbison.y"
{yyval.Expr = create_op_expr(UMinus,yyvsp[0].Expr,0);;
    break;}
case 34:
#line 122 "forbison.y"
{yyval.Expr = create_comp_expr(As ,yyvsp[-2].Id, yyvsp[0].Int, NULL);;
    break;}
case 35:
#line 123 "forbison.y"
{yyval.Expr = create_comp_expr(As, yyvsp[-2].Id, NULL, yyvsp[0].String);;
    break;}
case 36:
#line 124 "forbison.y"
{yyval.Expr = create_comp_expr(As, yyvsp[-2].Id, NULL, yyvsp[0].Id);;
    break;}
case 37:
#line 125 "forbison.y"
{yyval.Expr = create_comp_expr(GT, yyvsp[-2].Id, yyvsp[0].Int, NULL);;
    break;}
case 38:
#line 126 "forbison.y"
{yyval.Expr = create_comp_expr(LT, yyvsp[-2].Id, yyvsp[0].Int, NULL);;
    break;}
case 39:
#line 127 "forbison.y"
{yyval.Expr = create_comp_expr(GTOE, yyvsp[-2].Id, yyvsp[0].Int, NULL);;
    break;}
case 40:
#line 128 "forbison.y"
{yyval.Expr = create_comp_expr(LTOE, yyvsp[-2].Id, yyvsp[0].Int, NULL);;
    break;}
case 41:
#line 129 "forbison.y"
{yyval.Expr = create_comp_expr(NQ, yyvsp[-2].Id, NULL, yyvsp[0].Id);;
    break;}
case 42:
#line 130 "forbison.y"
{yyval.Expr = create_comp_expr(NQ, yyvsp[-2].Id, yyvsp[0].Int, NULL);;
    break;}
case 43:
#line 131 "forbison.y"
{yyval.Expr = create_comp_expr(NQ, yyvsp[-2].Id, NULL, yyvsp[0].String);;
    break;}
case 44:
#line 132 "forbison.y"
{yyval.Expr=yyvsp[-1].Expr;;
    break;}
case 45:
#line 134 "forbison.y"
{yyval.Stmt = create_dim_stmt(Integer_stmt, yyvsp[-3].Id, NULL, NULL);;
    break;}
case 46:
#line 135 "forbison.y"
{yyval.Stmt = create_dim_stmt(String_stmt, yyvsp[-3].Id, NULL, NULL);;
    break;}
case 47:
#line 136 "forbison.y"
{yyval.Stmt = create_dim_stmt(Integer_stmt, yyvsp[-5].Id, yyvsp[-1].Int, NULL);;
    break;}
case 48:
#line 137 "forbison.y"
{yyval.Stmt = create_dim_stmt(String_stmt, yyvsp[-5].Id, NULL, yyvsp[-1].String);;
    break;}
case 49:
#line 139 "forbison.y"
{yyval.Stmt = create_mass_stmt(Integer_stmt, yyvsp[-5].Id, yyvsp[-3].Int);
    break;}
case 50:
#line 140 "forbison.y"
{yyval.Stmt = create_mass_stmt(String_stmt, yyvsp[-5].Id, yyvsp[-3].Int);
    break;}
case 51:
#line 142 "forbison.y"
{yyval.EL = create_expr_seq(yyvsp[0].Expr);;
    break;}
case 52:
#line 143 "forbison.y"
{yyval.EL = add_to_expr_seq(yyvsp[-2].EL,yyvsp[0].Expr);;
    break;}
case 53:
#line 145 "forbison.y"
{yyval.Stmt = create_if_stmt(yyvsp[-8].Expr, yyvsp[-5].SL, yyvsp[-4].SL, yyvsp[-3].Stmt);;
    break;}
case 54:
#line 146 "forbison.y"
{yyval.Stmt = create_if_stmt(yyvsp[-7].Expr, yyvsp[-4].SL, NULL, yyvsp[-3].Stmt);;
    break;}
case 55:
#line 148 "forbison.y"
{yyval.SL = add_to_elseif_list(yyvsp[-1].SL, yyvsp[0].Stmt);;
    break;}
case 56:
#line 149 "forbison.y"
{yyval.SL = create_elseif_list(yyvsp[0].Stmt);;
    break;}
case 57:
#line 151 "forbison.y"
{yyval.Stmt = create_else_stmt(yyvsp[-3].Expr, yyvsp[0].SL);;
    break;}
case 58:
#line 153 "forbison.y"
{yyval.Stmt = create_else_stmt(NULL, yyvsp[0].SL);;
    break;}
case 59:
#line 154 "forbison.y"
{yyval.Stmt = create_else_stmt(NULL, NULL);;
    break;}
case 60:
#line 156 "forbison.y"
{yyval.Stmt = create_while_stmt(yyvsp[-5].Expr, yyvsp[-3].SL);;
    break;}
case 61:
#line 158 "forbison.y"
{yyval.Stmt = create_dim_stmt(Integer_stmt,yyvsp[-2].Id,NULL,NULL);;
    break;}
case 62:
#line 159 "forbison.y"
{yyval.Stmt = create_dim_stmt(String_stmt,yyvsp[-2].Id,NULL,NULL);;
    break;}
case 63:
#line 161 "forbison.y"
{yyval.SL = add_to_stmt_list(yyvsp[-2].SL, yyvsp[0].Stmt);;
    break;}
case 64:
#line 162 "forbison.y"
{yyval.SL = create_stmt_list(yyvsp[0].Stmt);;
    break;}
case 65:
#line 164 "forbison.y"
{yyval.Stmt = create_console(Write ,yyvsp[-2].Expr);;
    break;}
case 66:
#line 166 "forbison.y"
{yyval.Stmt = create_console(Read ,NULL);;
    break;}
case 67:
#line 168 "forbison.y"
{yyval.Stmt = create_fun_stmt(yyvsp[-10].Id, yyvsp[-8].SL, Fun_Integer, yyvsp[-3].SL);;
    break;}
case 68:
#line 169 "forbison.y"
{yyval.Stmt = create_fun_stmt(yyvsp[-10].Id, yyvsp[-8].SL, Fun_String, yyvsp[-3].SL);;
    break;}
case 70:
#line 171 "forbison.y"
{yyval.Stmt = create_sub_stmt(Sub ,yyvsp[-8].Id, yyvsp[-6].SL, yyvsp[-3].SL);;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 487 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 174 "forbison.y"

int main(int argc, char *argv[])
{
	if(argc<=1)
	{
		puts("Error");
	}
	else
	{
		fopen_s(&yyin, argv[1], "r");
		yyparse();
		graph();
	}
	_getch();
	return 0;
}
struct Program * create_program(struct StatementList *lst)
{
	struct Program * Result =(struct Program *)malloc(sizeof(struct Program));
	Result->stmts=lst;
	return Result;
}
struct Expression * create_const_expr(int token)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Int_const;
Result->Num = token;
Result->Name = NULL;
Result->Str = NULL;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Expression * create_id_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Id;
Result->Num = NULL;
Result->Name = token;
Result->Str = NULL;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Expression * create_string_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Str_const;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = token;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Type;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Left = lhs;
Result->Right = rhs;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr = expr;
Result->Block = NULL;
Result->Arg = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block, struct StatementList *elseif_block, struct Statement *else_stmt)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=If;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr= expr;
Result->Block=block;
Result->Arg=NULL;
Result->ElseIfList=elseif_block;
Result->ElseStmt=else_stmt;
Result->Next=NULL;
return Result;
}
struct StatementList * create_stmt_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}
struct StatementList * add_to_stmt_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}
struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=While;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr=expr;
Result->Block=block;
Result->Arg=NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next=NULL;
return Result;
}
struct Statement * create_fun_stmt(char * name ,struct StatementList *arg, enum StmtType type, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = name;
Result->Str = NULL;
Result->Expr = NULL;
Result->Block = block;
Result->Arg = arg;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Statement * create_sub_stmt(enum StmtType type ,char * name ,struct StatementList *arg, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = name;
Result->Str = NULL;
Result->Expr = NULL;
Result->Block = block;
Result->Arg = arg;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Statement * create_console(enum StmtType type, struct Expression * e1)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr = e1;
Result->Block = NULL;
Result->Arg = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Expression * create_mass_expr(char * token, int num)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type=Mass;
Result->Num=num;
Result->Name=token;
Result->Str=NULL;
Result->Left=NULL;
Result->Right=NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct StatementList * create_fun_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}
struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}
struct ExpressionList * create_expr_seq(struct Expression *e1)
{
struct ExpressionList *Result = (struct ExpressionList *) malloc(sizeof(struct ExpressionList));
Result->First=e1;
Result->Last=e1;
return Result;
}
struct ExpressionList * add_to_expr_seq(struct ExpressionList *e1, struct Expression *e2)
{
e1->Last->Next=e2;
e1->Last=e2;
return e1;
}
struct Expression * create_comp_expr(enum ExprType type, char * token, int * num, char * str)
{
struct Expression *Result = (struct Expression *) malloc(sizeof(struct Expression));
Result->Type = type;
Result->Num = num;
Result->Name = token;
Result->Str = str;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Statement * create_dim_stmt(enum StmtType type, char * token, int num, char *str)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = num;
Result->Name = token;
Result->Str = str;
Result->Expr = NULL;
Result->Block = NULL;
Result->Arg = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct StatementList * create_elseif_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}
struct StatementList * add_to_elseif_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}
struct Statement * create_else_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = AltIf;
Result->Name = NULL;
Result->Num = NULL;
Result->Str = NULL;
Result->Expr = expr;
Result->Block = block;
Result->Arg = NULL;
Result->Next = NULL;
Result->ElseIfList = NULL;
Result->ElseStmt = NULL;
return Result;
}
struct Statement * create_mass_stmt(enum StmtType type, char *token, int *num)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Name = token;
Result->Str = NULL;
Result->Num = num;
Result->Expr = NULL;
Result->Block = NULL;
Result->Arg = NULL;
Result->Next = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
return Result;
}
struct Expression * create_fun_expr(char * token, struct ExpressionList *seq)
{
struct Expression *Result = (struct Expression *) malloc(sizeof(struct Expression));
Result->Num = NULL;
Result->Name = token;
Result->Str = NULL;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = seq;
return Result;
}
void yyerror(char const *s)
{
 printf("%s",s);
}
