%{
# include "stdafx.h"
#include "tree_structs_for_bison.h"
#include <stdio.h>
#include "print_graph.h"
#include <malloc.h>
void yyerror(char const *s);
extern FILE *yyin;
extern int yylex(void);
struct Expression * create_const_expr(int token);
struct Expression * create_id_expr(char * token);
struct Expression * create_string_expr(char * token);
struct Expression * create_mass_expr(char * token, int num);
struct Expression * create_comp_expr(enum ExprType type, char * token, int * num, char * str);
struct Expression * create_fun_expr(char *token, struct ExpressionList *seq);
struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs);
struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr);
struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block, struct StatementList *elseif_block, struct Statement *else_stmt);
struct Statement * create_sub_stmt(enum StmtType type, char * name ,struct StatementList *arg, struct StatementList *block);
struct Statement * create_dim_stmt(enum StmtType type, char * token);
struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block);
struct Statement * create_fun_stmt(char * name ,struct StatementList *arg, enum StmtType type1, struct StatementList *block);
struct Statement * create_console(enum StmtType type, struct Expression * e1);
struct Statement * create_else_stmt(struct Expression *expr, struct StatementList *block);
struct Statement * create_mass_stmt(enum StmtType type, char *token, int *num);
struct StatementList * create_stmt_list(struct Statement *s1);
struct StatementList * add_to_stmt_list(struct StatementList *s1, struct Statement *s2);
struct StatementList * create_fun_list(struct Statement *s1);
struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2);
struct StatementList * create_elseif_list(struct Statement *s1);
struct StatementList * add_to_elseif_list(struct StatementList *s1, struct Statement *s2);
struct ExpressionList * add_to_expr_seq(struct ExpressionList *e1, struct Expression *e2);
struct ExpressionList * create_expr_seq(struct Expression *e1);
struct Program *create_program(struct StatementList *lst);
struct Program *Prg;
%}
%union {
int Int;
char *Id;
char *String;
char* str;
int ival;
struct Expression *Expr;
struct StatementList *SL;
struct Statement *Stmt;
struct Program *Prg;
struct ExpressionList *EL;
};
%type <Expr> expr comp_expr
%type <SL> stmt_list fun_list elseif_list arg_list
%type <EL> expr_seq
%type <Stmt> stmt if_stmt while_stmt fun_stmt print_stmt scanf_stmt dim_stmt elseif_stmt else_stmt mass_stmt arg_fun
%type <Prg> program
%token <Int> INT
%token <Id> ID
%token <String> STRING_VB_CONST
%token ENDL
%token IF
%token THEN
%token END
%token ELSE
%token ELSEIF
%token WHILE
%token AS
%token DIM
%token INTEGER
%token STRING_VB
%token FUNCTION_VB
%token SUB
%token CONSOLE
%token READLINE
%token WRITELINE
%token RETURN
%token GREATER_THAN_OR_EQUAL
%token LESS_THAN_OR_EQUAL
%token NOT_EQUAL
%right '='
%left '<' '>' NOT_EQUAL GREATER_THAN_OR_EQUAL LESS_THAN_OR_EQUAL
%left '+' '-'
%left '*' '/'
%left UMINUS INT
%nonassoc '(' ')'
%%
program : fun_list {Prg=create_program($1); $$=$1;}
;
fun_list : fun_stmt {$$ = create_fun_list($1);}
| fun_list fun_stmt {$$ = add_to_fun_list($1, $2);}
;
stmt_list : stmt {$$=create_stmt_list($1);}
| stmt_list stmt {$$=add_to_stmt_list ($1, $2);}
| stmt_list ENDL {$$=$1;}
;
stmt : expr ENDL {$$=create_simple_stmt(Ex,$1);}
| if_stmt {$$=$1;}
| while_stmt {$$=$1;}
| print_stmt {$$=$1;}
| scanf_stmt {$$=$1;}
| RETURN expr ENDL {$$=create_simple_stmt(Return_stmt, $2);}
| RETURN ENDL {$$=create_simple_stmt(Return_stmt, NULL);}
| dim_stmt {$$=$1;}
| mass_stmt {$$=$1;}
;
expr : INT {$$=create_const_expr($1);}
| ID {$$=create_id_expr($1);}
| STRING_VB_CONST {$$=create_string_expr($1);}
| expr '=' expr {$$=create_op_expr(As,$1,$3);}
| expr '+' expr {$$=create_op_expr(Plus,$1,$3);}
| expr '-' expr {$$=create_op_expr(Minus,$1,$3);}
| expr '*' expr {$$=create_op_expr(Mul,$1,$3);}
| expr '/' expr {$$=create_op_expr(Div,$1,$3);}
| expr '>' expr {$$=create_op_expr(GT,$1,$3);}
| expr '<' expr {$$=create_op_expr(LT,$1,$3);}
| expr GREATER_THAN_OR_EQUAL expr {$$=create_op_expr(GTOE,$1,$3);}
| expr LESS_THAN_OR_EQUAL expr {$$=create_op_expr(LTOE,$1,$3);}
| expr NOT_EQUAL expr {$$=create_op_expr(NQ,$1,$3);}
| ID '(' expr_seq ')' {$$=create_fun_expr($1, $3);}
| ID '(' ')' {$$=create_fun_expr($1, NULL);}
| ID '(' INT ')' {$$=create_mass_expr($1, $3);}
|'(' expr ')' {$$=$2;}
| '-' expr %prec UMINUS {$$ = create_op_expr(UMinus,$2,0);}
;
comp_expr : ID '=' INT {$$ = create_comp_expr(As ,$1, $3, NULL);}
| ID '=' STRING_VB_CONST {$$ = create_comp_expr(As, $1, NULL, $3);}
| ID '=' ID {$$ = create_comp_expr(As, $1, NULL, $3);}
| ID '>' INT {$$ = create_comp_expr(GT, $1, $3, NULL);}
| ID '<' INT {$$ = create_comp_expr(LT, $1, $3, NULL);}
| ID GREATER_THAN_OR_EQUAL INT {$$ = create_comp_expr(GTOE, $1, $3, NULL);}
| ID LESS_THAN_OR_EQUAL INT {$$ = create_comp_expr(LTOE, $1, $3, NULL);}
| ID NOT_EQUAL ID {$$ = create_comp_expr(NQ, $1, NULL, $3);}
| ID NOT_EQUAL INT {$$ = create_comp_expr(NQ, $1, $3, NULL);}
| ID NOT_EQUAL STRING_VB_CONST {$$ = create_comp_expr(NQ, $1, NULL, $3);}
| comp_expr ENDL {$$=$1;}
;
dim_stmt: DIM ID AS INTEGER ENDL {$$ = create_dim_stmt(Integer_stmt, $2, NULL, NULL);}
| DIM ID AS STRING_VB ENDL {$$ = create_dim_stmt(String_stmt, $2, NULL, NULL);}
| DIM ID AS INTEGER '=' INT ENDL {$$ = create_dim_stmt(Integer_stmt, $2, $6, NULL);}
| DIM ID AS STRING_VB '=' STRING_VB_CONST ENDL {$$ = create_dim_stmt(String_stmt, $2, NULL, $6);}
;
mass_stmt: DIM ID '(' INT ')' AS INTEGER {$$ = create_mass_stmt(Integer_stmt, $2, $4)}
| DIM ID '(' INT ')' AS STRING_VB {$$ = create_mass_stmt(String_stmt, $2, $4)}
;
expr_seq: expr {$$ = create_expr_seq($1);}
| expr_seq ',' expr {$$ = add_to_expr_seq($1,$3);}
;
if_stmt: IF comp_expr THEN ENDL stmt_list elseif_list else_stmt END IF ENDL {$$ = create_if_stmt($2, $5, $6, $7);}
| IF comp_expr THEN ENDL stmt_list else_stmt END IF ENDL {$$ = create_if_stmt($2, $5, NULL, $6);}
;
elseif_list: elseif_list elseif_stmt {$$ = add_to_elseif_list($1, $2);}
| elseif_stmt {$$ = create_elseif_list($1);}
;
elseif_stmt: ELSEIF comp_expr THEN ENDL stmt_list {$$ = create_else_stmt($2, $5);}
;
else_stmt: ELSE ENDL stmt_list {$$ = create_else_stmt(NULL, $3);}
| {$$ = create_else_stmt(NULL, NULL);}
;
while_stmt: WHILE comp_expr ENDL stmt_list END WHILE ENDL {$$ = create_while_stmt($2, $4);}
;
arg_fun : ID AS INTEGER {$$ = create_dim_stmt(Integer_stmt,$1,NULL,NULL);}
| ID AS STRING_VB {$$ = create_dim_stmt(String_stmt,$1,NULL,NULL);}
;
arg_list: arg_list ',' arg_fun {$$ = add_to_stmt_list($1, $3);}
| arg_fun {$$ = create_stmt_list($1);}
;
print_stmt : CONSOLE '.' WRITELINE '(' expr ')' ENDL {$$ = create_console(Write ,$5);}
;
scanf_stmt : CONSOLE '.' READLINE '(' ')' ENDL  {$$ = create_console(Read ,NULL);}
;
fun_stmt: FUNCTION_VB ID '(' arg_list ')' AS INTEGER ENDL stmt_list END FUNCTION_VB ENDL {$$ = create_fun_stmt($2, $4, Fun_Integer, $9);}
| FUNCTION_VB ID '(' arg_list ')' AS STRING_VB ENDL stmt_list END FUNCTION_VB ENDL {$$ = create_fun_stmt($2, $4, Fun_String, $9);}
| fun_stmt ENDL 
| SUB ID '(' arg_list ')' ENDL stmt_list END SUB ENDL {$$ = create_sub_stmt(Sub ,$2, $4, $7);}
;
 
%%
int main(int argc, char *argv[])
{
	if(argc<=1)
	{
		puts("Error");
	}
	else
	{
		fopen_s(&yyin, argv[1], "r");
		yyparse();
		graph();
	}
	_getch();
	return 0;
}
struct Program * create_program(struct StatementList *lst)
{
	struct Program * Result =(struct Program *)malloc(sizeof(struct Program));
	Result->stmts=lst;
	return Result;
}
struct Expression * create_const_expr(int token)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Int_const;
Result->Num = token;
Result->Name = NULL;
Result->Str = NULL;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Expression * create_id_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Id;
Result->Num = NULL;
Result->Name = token;
Result->Str = NULL;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Expression * create_string_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Str_const;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = token;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Type;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Left = lhs;
Result->Right = rhs;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr = expr;
Result->Block = NULL;
Result->Arg = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block, struct StatementList *elseif_block, struct Statement *else_stmt)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=If;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr= expr;
Result->Block=block;
Result->Arg=NULL;
Result->ElseIfList=elseif_block;
Result->ElseStmt=else_stmt;
Result->Next=NULL;
return Result;
}
struct StatementList * create_stmt_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}
struct StatementList * add_to_stmt_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}
struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=While;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr=expr;
Result->Block=block;
Result->Arg=NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next=NULL;
return Result;
}
struct Statement * create_fun_stmt(char * name ,struct StatementList *arg, enum StmtType type, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = name;
Result->Str = NULL;
Result->Expr = NULL;
Result->Block = block;
Result->Arg = arg;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Statement * create_sub_stmt(enum StmtType type ,char * name ,struct StatementList *arg, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = name;
Result->Str = NULL;
Result->Expr = NULL;
Result->Block = block;
Result->Arg = arg;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Statement * create_console(enum StmtType type, struct Expression * e1)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = NULL;
Result->Name = NULL;
Result->Str = NULL;
Result->Expr = e1;
Result->Block = NULL;
Result->Arg = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct Expression * create_mass_expr(char * token, int num)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type=Mass;
Result->Num=num;
Result->Name=token;
Result->Str=NULL;
Result->Left=NULL;
Result->Right=NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct StatementList * create_fun_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}
struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}
struct ExpressionList * create_expr_seq(struct Expression *e1)
{
struct ExpressionList *Result = (struct ExpressionList *) malloc(sizeof(struct ExpressionList));
Result->First=e1;
Result->Last=e1;
return Result;
}
struct ExpressionList * add_to_expr_seq(struct ExpressionList *e1, struct Expression *e2)
{
e1->Last->Next=e2;
e1->Last=e2;
return e1;
}
struct Expression * create_comp_expr(enum ExprType type, char * token, int * num, char * str)
{
struct Expression *Result = (struct Expression *) malloc(sizeof(struct Expression));
Result->Type = type;
Result->Num = num;
Result->Name = token;
Result->Str = str;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = NULL;
return Result;
}
struct Statement * create_dim_stmt(enum StmtType type, char * token, int num, char *str)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Num = num;
Result->Name = token;
Result->Str = str;
Result->Expr = NULL;
Result->Block = NULL;
Result->Arg = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
Result->Next = NULL;
return Result;
}
struct StatementList * create_elseif_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}
struct StatementList * add_to_elseif_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}
struct Statement * create_else_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = AltIf;
Result->Name = NULL;
Result->Num = NULL;
Result->Str = NULL;
Result->Expr = expr;
Result->Block = block;
Result->Arg = NULL;
Result->Next = NULL;
Result->ElseIfList = NULL;
Result->ElseStmt = NULL;
return Result;
}
struct Statement * create_mass_stmt(enum StmtType type, char *token, int *num)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type = type;
Result->Name = token;
Result->Str = NULL;
Result->Num = num;
Result->Expr = NULL;
Result->Block = NULL;
Result->Arg = NULL;
Result->Next = NULL;
Result->ElseIfList=NULL;
Result->ElseStmt=NULL;
return Result;
}
struct Expression * create_fun_expr(char * token, struct ExpressionList *seq)
{
struct Expression *Result = (struct Expression *) malloc(sizeof(struct Expression));
Result->Num = NULL;
Result->Name = token;
Result->Str = NULL;
Result->Left = NULL;
Result->Right = NULL;
Result->Next = NULL;
Result->ExprList = seq;
return Result;
}
void yyerror(char const *s)
{
 printf("%s",s);
}
