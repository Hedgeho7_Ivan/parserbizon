void graph();
void print_expr(struct Expression *expr, int lst_id);
void print_stmt(struct Statement *stmt, int lst_id);
void print_stmt_lst(struct StatementList *stmts);
void print_expr_lst(struct StatementList *stmts);

int next = 0;
int el_count = 0;
FILE *pic;
extern struct Program *Prg;
extern enum ExprType;
extern enum StmtType;

// ��������� �����
void graph()
{
	el_count++;

	pic = fopen("graph.txt", "a");
	fprintf(pic, "root -> stmt_list_%d;\n", el_count);
	fclose(pic);
	
	print_stmt_lst(Prg->stmts);
}

void print_stmt_lst(struct StatementList *stmts)
{
	int Id_point = el_count;
	if (stmts->First)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_list_%d -> stmt_%d;\n", Id_point, el_count);
		fclose(pic);
			
		print_stmt(stmts->First, Id_point);
	}
}

void print_expr_lst(struct ExpressionList *exprs)
{
	int Id_point = el_count;
	if (exprs->First)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_list_%d -> expr_%d;\n", Id_point, el_count);
		fclose(pic);
			
		print_expr(exprs->First, Id_point);
	}
}

void print_stmt(struct Statement *stmt, int lst_id)
{
	int Id_point = el_count;
	if (stmt->Type == Ex)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Expr_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == If)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_If_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == AltIf)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_AltIf_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == While)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_While_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == Write)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Write_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == Read)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Read_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == Sub)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Sub_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == Fun_Integer)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Fun_Integer_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == Fun_String)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Fun_String_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == Integer_stmt)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_Integer_stmt_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == String_stmt)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_String_stmt_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (stmt->Type == RETURN)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Type_RETURN_%d;\n", Id_point, el_count);
		fclose(pic);
	}

	if (stmt->Name)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Name_%s_%d;\n", Id_point, stmt->Name, el_count);
		fclose(pic);
	}
	if (stmt->Str)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Str_%s_%d;\n", Id_point, stmt->Str, el_count);
		fclose(pic);
	}
	if (stmt->Num)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> Num_%d_%d;\n", Id_point, stmt->Num, el_count);
		fclose(pic);
	}
	if (stmt->Expr)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> expr_%d;\n", Id_point, el_count);
		fclose(pic);

		print_expr(stmt->Expr, Id_point);
	}
	if (stmt->Block)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> stmt_list_%d;\n", Id_point, el_count);
		fclose(pic);

		print_stmt_lst(stmt->Block);
	}
	if (stmt->Arg)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> stmt_list_%d;\n", Id_point, el_count);
		fclose(pic);

		print_stmt_lst(stmt->Arg);
	}
	if (stmt->ElseIfList)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> stmt_list_%d;\n", Id_point, el_count);
		fclose(pic);

		print_stmt_lst(stmt->ElseIfList);
	}
	if (stmt->ElseStmt)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_%d -> stmt_%d;\n", Id_point, el_count);
		fclose(pic);

		print_stmt(stmt->ElseStmt, Id_point);
	}
	if (stmt->Next)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "stmt_list_%d -> stmt_%d;\n", lst_id, el_count);
		fclose(pic);

		print_stmt(stmt->Next, lst_id);
	}
}

void print_expr(struct Expression *expr, int lst_id)
{
	int Id_point = el_count;
	int lr_id;
	if (expr->Type == Int_const)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> Type_Int_const_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Id)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> Type_Id_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Str_const)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> Type_Str_const_%d;\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Plus)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_+_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Minus)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_-_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Mul)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_*_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Div)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_/_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == As)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_=_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == LT)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_<_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == GT)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_>_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == UMinus)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_U-_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Integer)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_Integer_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == String)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_String_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == LTOE)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_<=_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == GTOE)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_>=_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == NQ)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_<>_%d\";\n", Id_point, el_count);
		fclose(pic);
	}
	else if (expr->Type == Mass)
	{

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> \"Type_Mass_%d\";\n", Id_point, el_count);
		fclose(pic);
	}

	if (expr->Num)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> Num_%d_%d;\n", Id_point, expr->Num, el_count);
		fclose(pic);
	}
	if (expr->Name)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> Name_%s_%d;\n", Id_point, expr->Name, el_count);
		fclose(pic);
	}
	if (expr->Str)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> Str_%s_%d;\n", Id_point, expr->Str, el_count);
		fclose(pic);
	}
	if (expr->Left)
	{	
		el_count++;
		lr_id = el_count;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> expr_%d;\n", Id_point, el_count);
		fclose(pic);

		print_expr(expr->Left, Id_point);

		if (expr->Right)
		{
			el_count++;

			pic = fopen("graph.txt", "a");
			fprintf(pic, "expr_%d -> expr_%d;\n", Id_point, el_count);
			fclose(pic);

			print_expr(expr->Right, Id_point);
		}
	}
	if (expr->Next)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_list_%d -> expr_%d;\n", lst_id, el_count);
		fclose(pic);

		print_expr(expr->Next, lst_id);
	}
	if (expr->ExprList)
	{
		el_count++;

		pic = fopen("graph.txt", "a");
		fprintf(pic, "expr_%d -> expr_list_%d;\n", Id_point, el_count);
		fclose(pic);

		print_expr_lst(expr->ExprList);
	}
}