typedef union {
int Int;
char *Id;
char *String;
char* str;
int ival;
struct Expression *Expr;
struct StatementList *SL;
struct Statement *Stmt;
struct Program *Prg;
struct ExpressionList *EL;
} YYSTYPE;
#define	INT	258
#define	ID	259
#define	STRING_VB_CONST	260
#define	ENDL	261
#define	IF	262
#define	THEN	263
#define	END	264
#define	ELSE	265
#define	ELSEIF	266
#define	WHILE	267
#define	AS	268
#define	DIM	269
#define	INTEGER	270
#define	STRING_VB	271
#define	FUNCTION_VB	272
#define	SUB	273
#define	CONSOLE	274
#define	READLINE	275
#define	WRITELINE	276
#define	RETURN	277
#define	GREATER_THAN_OR_EQUAL	278
#define	LESS_THAN_OR_EQUAL	279
#define	NOT_EQUAL	280
#define	UMINUS	281


extern YYSTYPE yylval;
